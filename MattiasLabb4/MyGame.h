//
//  MyGame.h
//  MattiasLabb4
//
//  Created by IT-Högskolan on 2015-02-16.
//  Copyright (c) 2015 IT-Högskolan. All rights reserved.
//

#import <UIKit/UIKit.h>

int Y;
int X;

int Player1Points;
int Player2Points;

@interface MyGame : UIViewController
{
    
    IBOutlet UILabel *Player1Score;
    IBOutlet UILabel *Player2Score;
    IBOutlet UILabel *WinLose;
    IBOutlet UIButton *Exit;
    
}

-(IBAction)StartButton:(id)sender;
-(void)PuckMove;
-(void)Player2Movment;
-(void)Collision;


@end
