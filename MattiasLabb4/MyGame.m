//
//  MyGame.m
//  MattiasLabb4
//
//  Created by IT-Högskolan on 2015-02-16.
//  Copyright (c) 2015 IT-Högskolan. All rights reserved.
//

#import "MyGame.h"

@interface MyGame ()
@property (weak, nonatomic) IBOutlet UIImageView *player1;
@property (weak, nonatomic) IBOutlet UIImageView *player2;
@property (weak, nonatomic) IBOutlet UIImageView *puck;
@property (weak, nonatomic) IBOutlet UIButton *startButton;
@property (weak, nonatomic) NSTimer *timer;


@end

@implementation MyGame

-(void)Collision{
    
    if (CGRectIntersectsRect(self.puck.frame, self.player1.frame)) {
        
        Y = arc4random() %5;
        Y = 0-Y;
    }
    
    if (CGRectIntersectsRect(self.puck.frame, self.player2.frame)) {
        
        Y = arc4random() %5;
        
    }
    
}

-(void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event{
    
    UITouch *Drag = [[event allTouches]anyObject];
    self.player1.center = [Drag locationInView:self.view];
    
    
    if (self.player1.center.y > self.view.frame.size.height -65) {
        self.player1.center = CGPointMake(self.player1.center.x, self.view.frame.size.height -65);
    }
    
    if (self.player1.center.y < self.view.frame.size.height -65) {
        self.player1.center = CGPointMake(self.player1.center.x, self.view.frame.size.height -65);
    }
    
    if (self.player1.center.x < 40) {
        self.player1.center = CGPointMake(40, self.player1.center.y);
    }
    
    if (self.player1.center.x > self.view.frame.size.width -40) {
        self.player1.center = CGPointMake(self.view.frame.size.width -40, self.player1.center.y);
    }
    
}


-(void)Player2Movment{
    
    if (self.player2.center.x > self.puck.center.x) {
        self.player2.center = CGPointMake(self.player2.center.x -0.8, self.player2.center.y);
    }
    
    if(self.player2.center.x < self.puck.center.x){
        self.player2.center = CGPointMake(self.player2.center.x + 0.8, self.player2.center.y);
    }
    
    if (self.player2.center.x < 40) {
        self.player2.center = CGPointMake(40, self.player2.center.y);
    }
    
    if (self.player2.center.x > self.view.frame.size.width -40) {
        self.player2.center = CGPointMake(self.view.frame.size.width -40, self.player2.center.y);
    }
    
}

-(IBAction)StartButton:(id)sender{
    
    self.startButton.hidden = YES;
    Exit.hidden = YES;
    
    Y = arc4random() %11;
    Y = Y -5;
    
    X = arc4random() %11;
    X = X -5;
    
    if (Y <= 2) {
        Y = 3;
    }
    if (X <= 2) {
        X = 3;
    }
    
     self.timer = [NSTimer scheduledTimerWithTimeInterval:0.01 target:self selector:@selector(PuckMove) userInfo:nil repeats:YES];
    
    
}

-(void)PuckMove{
    
    [self Player2Movment];
    [self Collision];
    
    self.puck.center = CGPointMake(self.puck.center.x + X, self.puck.center.y + Y);
    
    if (self.puck.center.x < 15) {
        X = 0 - X;
    }
    if (self.puck.center.x > self.view.frame.size.width-15) {
        X = 0 - X;
    }
    
    if (self.puck.center.y < 0) {
        Player1Points = Player1Points +1;
        Player1Score.text = [NSString stringWithFormat:@"%i", Player1Points];
        
        [self.timer invalidate];
        self.startButton.hidden = NO;
        
        self.puck.center = CGPointMake(self.view.frame.size.width /2, self.view.frame.size.height /2);
        self.player2.center = CGPointMake(self.view.frame.size.width /2, 65);
        
        if (Player1Points == 5) {
            self.startButton.hidden = YES;
            Exit.hidden = NO;
            WinLose.hidden = NO;
            WinLose.text = [NSString stringWithFormat:@"You WiN!"];
        }
        
    }
    
    if (self.puck.center.y > self.view.frame.size.height-15) {
        Player2Points = Player2Points +1;
        Player2Score.text = [NSString stringWithFormat:@"%i", Player2Points];
        [self.timer invalidate];
        self.startButton.hidden = NO;
        self.puck.center = CGPointMake(self.view.frame.size.width /2, self.view.frame.size.height /2);
        self.player2.center = CGPointMake(self.view.frame.size.width /2, 65);
        
        if (Player2Points == 5) {
            self.startButton.hidden = YES;
            Exit.hidden = NO;
            WinLose.hidden = NO;
            WinLose.text = [NSString stringWithFormat:@"You LOOOSE!"];
            
        }
    }
}
-(void)viewDidLayoutSubviews {
    
    self.player1.center = CGPointMake(self.view.frame.size.width /2, self.view.frame.size.height -65);
    self.player2.center = CGPointMake(self.view.frame.size.width /2, 65);
    self.puck.center = CGPointMake(self.view.frame.size.width /2, self.view.frame.size.height /2);
    self.startButton.center = CGPointMake(self.view.frame.size.width /2, self.view.frame.size.height /2 +40);
}


- (void)viewDidLoad {
    [super viewDidLoad];
    Player1Points = 0;
    Player2Points = 0;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
